<?php

namespace App\Factories;

use Sqola\Entities\Student;
use Sqola\Contracts\UserFactory;

class DatabaseUserFactory implements UserFactory
{
    public function makeStudent($payload): Student
    {
        $id = \DB::table('users')->insertGetId([
            'name'     => $payload['name'],
            'email'    => $payload['email'],
            'password' => $payload['password']
        ]);

        return new Student($id, $payload['name'], $payload['email']);
    }
}
