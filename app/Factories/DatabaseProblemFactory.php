<?php

namespace App\Factories;

use Sqola\Entities\Problem;
use Sqola\Contracts\ProblemFactory;

class DatabaseProblemFactory implements ProblemFactory
{
    public function createProblem($payload): Problem
    {
        $id = \DB::table('problems')->insertGetId([
            'unit_id'   => $payload['unit_id'],
            'config_id' => $payload['config_id'],
            'title'     => $payload['title'],
        ]);

        $unit = new Unit;;

        return new Problem(
            $id, $unit, $payload['config_id'], $payload['title']
        );
    }
}
