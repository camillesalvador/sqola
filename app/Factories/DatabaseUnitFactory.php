<?php

namespace App\Factories;

use Sqola\Entities\Unit;
use Sqola\Contracts\UnitFactory;

class DatabaseUnitFactory implements UnitFactory
{
    public function createUnit($payload): Unit
    {
        $id = \DB::table('units')->insertGetId([
            'name' => $payload['name'],
            'slug' => $payload['slug']
        ]);

        return new Unit($id, $payload['name'], $payload['slug']);
    }
}
