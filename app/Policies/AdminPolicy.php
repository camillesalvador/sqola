<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
    }

    public function view(User $user): bool
    {
        return (bool) $user->is_admin;
    }
}
