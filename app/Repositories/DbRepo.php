<?php

namespace App\Repositories;

use Sqola\Contracts\Repository;
use Sqola\Entities\Answer;
use Sqola\Entities\Problem;
use Sqola\Entities\Student;
use Sqola\Entities\Unit;
use Sqola\Values\UnitConfig;
use Sqola\Values\CourseConfig;
use Sqola\Services\Course;

class DbRepo implements Repository
{
    public function saveStudent(Student $student)
    {
        \DB::table('users')
            ->where('id', $student->id)
            ->update([
                'name'  => $student->name,
                'email' => $student->email
            ]);
    }

    public function allStudents()
    {
        $result = \DB::table('users')->get();

        $students = $result->map(function ($result) {
            return new Student($result->id, $result->name, $result->email);
        });

        return $students;
    }

    public function studentWithId($id)
    {
        $result = \DB::table('users')->where('id', $id)->first();

        if (!$result) {
            throw new \Exception("Student with id '$id' not found.");
        }

        return new Student($id, $result->name, $result->email);
    }

    public function answersForStudent(Student $student)
    {
        // \DB::enableQueryLog();
        $results = \DB::table('answers as a')
            ->select('a.*', 'problems.title as question')
            ->join('problems', 'problems.id', '=', 'a.problem_id')
            ->where('user_id', $student->id)
            ->get();
        // \Log::error('---', [\DB::getQueryLog()]);
        return $results;
    }

    public function saveAnswer(Answer $answer)
    {
        \DB::table('answers')->insert([
            'user_id'    => $answer->student->id,
            'problem_id' => $answer->problem->id,
            'answer'     => $answer->answer
        ]);
    }

    public function createProblem($payload): Problem
    {
        $id = \DB::table('problems')->insertGetId([
            'unit_id'   => $payload['unit_id'],
            'config_id' => $payload['config_id'],
            'title'     => $payload['title'],
        ]);

        $unit = $this->unitWithId($payload['unit_id']);

        $question = new Problem(
            $id,
            $unit,
            $payload['config_id'],
            $payload['title']
        );

        $unit->addQuestion($question);

        return $question;
    }

    public function updateProblem(Problem $problem)
    {
        \DB::table('problems')
            ->where('id', $problem->id)
            ->update([
                'unit_id'   => $problem->unit_id,
                'config_id' => $problem->config_id,
                'title'     => $problem->title
            ]);
    }

    // public function problemWithTitle($title)
    // {
    //     $result = \DB::table('problems')->where('title', $title)->first();
    //
    //     if (!$result) {
    //         throw new \Exception("Problem with title '$title' not found.");
    //     }
    //
    //     return new Problem($title);
    // }

    public function problemWithId($id): Problem
    {
        $result = \DB::table('problems')->where('id', $id)->first();

        if (!$result) {
            throw new \Exception("Problem with id '$id' not found.");
        }

        $unit = $this->unitWithId($result->unit_id);

        return new Problem($result->id, $unit, $result->config_id, $result->title);
    }

    public function questionWithConfigId($configId): Problem
    {
        $result = \DB::table('problems')->where('config_id', $configId)->first();

        if (!$result) {
            var_dump(\DB::table('problems')->get()->toArray());
            throw new \Exception("Problem with config_id '$configId' not found.");
        }

        $unit = $this->unitWithId($result->unit_id);

        return new Problem($result->id, $unit, $result->config_id, $result->title);
    }

    public function createUnit($unitName, UnitConfig $config): Unit
    {
        $id = \DB::table('units')->insertGetId([
            'name' => $unitName,
            'slug' => str_slug($unitName)
        ]);

        return new Unit($id, $config, $unitName, str_slug($unitName));
    }

    public function unitWithId($id)
    {
        $result = \DB::table('units')->where('id', $id)->first();

        if (!$result) {
            throw new \Exception("Unit with id '$id' not found.");
        }

        $config = $this->unitConfigWithName($result->slug);

        $unit = new Unit($id, $config, $result->name, $result->slug);

        $this->hydrateUnit($unit);

        return $unit;
    }

    public function unitWithName($name)
    {
        $result = \DB::table('units')->where('name', $name)->first();

        if (!$result) {
            throw new \Exception("Unit with name '$name' not found.");
        }

        $config = $this->unitConfigWithName($result->slug);

        $unit = new Unit($result->id, $config, $result->name, $result->slug);

        $this->hydrateUnit($unit);

        return $unit;
    }

    public function hydrateUnit(Unit $unit)
    {
        $results = \DB::table('problems')->where('unit_id', $unit->id)->get()->all();

        foreach ($results as $result) {
            $question = new Problem($result->id, $unit, $result->config_id, $result->title);

            $unit->addQuestion($question);
        }
    }

    public function unitWithSlug($slug): Unit
    {
        $result = \DB::table('units')->where('slug', $slug)->first();

        if (!$result) {
            throw new \Exception("Unit with slug '$slug' not found.");
        }

        $config = $this->unitConfigWithName($result->slug);

        $unit = new Unit($result->id, $config, $result->name, $result->slug);

        $this->hydrateUnit($unit);

        return $unit;
    }

    public function questionsWithUnitId($id)
    {
        $results = \DB::table('problems')->where('unit_id', $id)->get()->all();

        if (!$results) {
            throw new \Exception("Problems with unit_id '$id' not found.");
        }

        return $results;
    }

    public function getUnits()
    {
        $results = \DB::table('units')->get()->all();

        // if (!$results) {
        //     throw new \Exception("There are currently no units.");
        // }

        return $results;
    }

    public function unitConfigWithName($name): UnitConfig
    {
        $file = base_path('resources/units/'.$name.'.php');

        if (!file_exists($file)) {
            throw new \Exception("No unit config found for '$name' at $file");
        }

        $config = include $file;

        return new UnitConfig($config);
    }

    public function getCourse(): Course
    {
        $config = new CourseConfig([
            $this->unitWithName('syntax'),
            $this->unitWithName('loops'),
        ]);

        $course = new Course($this, $config);

        return $course;
    }
}
