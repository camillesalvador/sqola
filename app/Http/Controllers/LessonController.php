<?php

namespace App\Http\Controllers;

use App\Models\Lesson;
use App\Repositories\DbRepo;
use Illuminate\Http\Request;
use Sqola\Commands\AnswerProblem;
use Sqola\Commands\GetLesson;

class LessonController extends Controller
{
    public function show(Request $request, $unitSlug, $lessonIndex)
    {
        $this->repo = new DbRepo();
        $studentId  = $request->user()->id;

        $createLesson = new GetLesson($this->repo);
        $lesson       = $createLesson->execute([
            'studentId'   => $studentId,
            'unitName'    => $unitSlug,
            'lessonIndex' => $lessonIndex
        ]);

        $units = [
            'syntax'     => 'Syntax',
            'strings'    => 'Strings',
            'loops'      => 'Loops',
            'functions'  => 'Functions',
            'arrays'     => 'Arrays',
            'dates'      => 'Dates',
            'statements' => 'Statements',
            'objects'    => 'Objects',
            'algorithms' => 'Algorithms',
        ];

        $expected      = $lesson['expected'];
        $input         = $lesson['input'];
        $scenario      = $lesson['scenario'];
        $startingPoint = "";

        return view('units.show', [
            'input'         => $input,
            'unit'          => $units[$unitSlug],
            'lesson'        => $lessonIndex,
            'expected'      => $expected,
            'preamble'      => $lesson['preamble'],
            'startingPoint' => $startingPoint,
            'scenario'      => $scenario
        ]);
    }

    public function post(Request $request, $unitSlug, $questionIndex)
    {
        $this->repo = new DbRepo();
        $studentId  = $request->user()->id;

        $answerProblem = new AnswerProblem($this->repo);
        $response      = $answerProblem->execute([
            'student_id'    => $studentId,
            'questionIndex' => $questionIndex,
            'answer'        => $request->input('code'),
            'unitSlug'      => $unitSlug
        ]);

        if (!($response['unit'] ?? false)) {
            return [
                'success'   => $response['success'],
                'output'    => $response['output'],
                'nextRoute' => route('complete'),
                // 'next' => [
                //     'unit'   => $response['unit'],
                //     'lesson' => $response['lesson']
                // ]
            ];
        }

        return [
            'success'   => $response['success'],
            'output'    => $response['output'],
            'nextRoute' => route('units', [
                'unit'   => $response['unit'],
                'lesson' => $response['lesson']
            ]),
            'next' => [
                'unit'   => $response['unit'],
                'lesson' => $response['lesson']
            ]
        ];
    }

    public function complete()
    {
        return view('units.complete');
    }

    private function renderInput($input)
    {
        $i = '';
        foreach ($input as $key => $value) {
            $i .= "\n".'$'.$key .'=\''. $value .'\';'."\n";
        }

        return $i;
    }
}
