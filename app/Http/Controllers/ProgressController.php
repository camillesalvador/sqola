<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Repositories\DbRepo;
use Sqola\Commands\GetStudentReport;
use Sqola\Commands\GetAllStudentReports;

class ProgressController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($studentId)
    {
        $repo      = new DbRepo;
        $getReport = new GetStudentReport($repo);

        $report    = $getReport->execute([
            'id' => $studentId
        ]);

        return view('progress.show', [
            'report' => $report
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repo       = new DbRepo;
        $getReports = new GetAllStudentReports($repo);

        $report     = $getReports->execute();

        return view('progress.index', [
            'report' => $report
        ]);
    }
}
