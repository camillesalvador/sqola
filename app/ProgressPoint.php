<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ProgressPoint extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'unit_id', 'question_id', 'attempts', 'completed'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
