<?php
namespace Deployer;

require 'recipe/laravel.php';

// Configuration

set('repository', 'git@bitbucket.org:meriial/sqola.git');
set('git_tty', true); // [Optional] Allocate tty for git on first deployment
add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);


// Hosts

host('sqola.com')
    ->stage('prod')
    ->user('ec2-user')
    ->identityFile('~/keys/tsm-aws.pem')
    ->set('deploy_path', '/var/www/sqola.com');

// Tasks
task('artisan:cache:clear', function() {});
task('artisan:config:cache', function() {});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

// before('deploy:symlink', 'artisan:migrate');
