<?php

return PhpCsFixer\Config::create()
    ->setRules([
        '@PSR2'                  => true,
        'array_syntax'           => ['syntax' => 'short'],
        'no_unused_imports'      => true,
        'ordered_imports'        => true,
        'binary_operator_spaces' => [
            'align_double_arrow' => true,
            'align_equals'       => true
        ]
    ]);
