<?php

namespace Sqola\Contracts;

use Sqola\Entities\Student;

interface UserFactory
{
    public function makeStudent($payload): Student;
}
