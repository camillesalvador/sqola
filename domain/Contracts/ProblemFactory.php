<?php

namespace Sqola\Contracts;

use Sqola\Entities\Problem;

interface ProblemFactory
{
    public function createProblem($payload): Problem;
}
