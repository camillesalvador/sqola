<?php

namespace Sqola\Contracts;

use Sqola\Entities\Unit;

interface UnitFactory
{
    public function createUnit($payload): Unit;
}
