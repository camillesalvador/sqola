<?php

namespace Sqola\Entities;

use Sqola\Entities\Answer;
use Sqola\Services\ProgressReport;

class Student
{
    public function __construct($id, $name, $email)
    {
        // \Log::error('making new student with id '.$id);
        $this->id    = $id;
        $this->name  = $name;
        $this->email = $email;
    }

    public function answer(Problem $problem, string $answer)
    {
        return new Answer($problem, $this, $answer);
    }

    public function answers()
    {
        return $this->answers;
    }

    public function progress()
    {
        return new ProgressReport($this);
    }

    //
    // public function markQuestionComplete($question)
    // {
    //     $this->user->markCompleted($unit, $question);
    // }
    //
    // public function getLatestProgressPoint()
    // {
    //     $this->user->mostRecentProgressPoint();
    // }
}
