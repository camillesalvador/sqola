<?php

namespace Sqola\Commands;

class GetUnitByName extends Command
{
    public function execute($payload)
    {
        $unit = $this->repo->unitWithName($payload['name']);

        return $unit;
    }
}
