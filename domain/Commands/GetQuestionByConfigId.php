<?php

namespace Sqola\Commands;

class GetQuestionByConfigId extends Command
{
    public function execute($payload)
    {
        $question = $this->repo->problemWithConfigId($payload['config_id']);

        return (array) $question;
    }
}
