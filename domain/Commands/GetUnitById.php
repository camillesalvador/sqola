<?php

namespace Sqola\Commands;

class GetUnitById extends Command
{
    public function execute($payload)
    {
        $unit = $this->repo->unitWithId($payload['id']);

        return $unit;
    }
}
