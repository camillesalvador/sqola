<?php

namespace Sqola\Commands;

class GetQuestion extends Command
{
    public function execute($payload)
    {
        $question = $this->repo->problemWithId($payload['question_id']);

        return (array) $question;
    }
}
