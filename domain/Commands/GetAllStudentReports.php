<?php

namespace Sqola\Commands;

use Sqola\Services\ProgressSummary;

class GetAllStudentReports extends Command
{
    public function execute($payload = [])
    {
        $students = $this->repo->allStudents();

        $progressSummary = new ProgressSummary($this->repo);

        return (object) [
            'students' => $progressSummary->students()
        ];
    }
}
