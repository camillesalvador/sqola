<?php

namespace Sqola\Commands;
use Sqola\Services\ProgressReport;

class GetStudentReport extends Command
{
    public function execute($payload)
    {
        $student = $this->repo->studentWithId($payload['id']);

        $progressReport = new ProgressReport($this->repo, $student);

        return (object) [
            'answeredQuestions' => $progressReport->answeredQuestions()
        ];
    }
}
