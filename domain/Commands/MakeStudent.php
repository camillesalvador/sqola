<?php

namespace Sqola\Commands;

use Sqola\Entities\Student;
use Sqola\Contracts\Repository;
use Sqola\Contracts\UserFactory;

class MakeStudent extends Command
{
    public function __construct(Repository $repo, UserFactory $userFactory)
    {
        $this->repo = $repo;
        $this->userFactory = $userFactory;
    }

    public function execute($payload)
    {
        $student = $this->userFactory->makeStudent($payload);

        $this->repo->saveStudent($student);

        return $student->id;
    }
}
