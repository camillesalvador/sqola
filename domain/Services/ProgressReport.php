<?php

namespace Sqola\Services;

use Sqola\Contracts\Repository;
use Sqola\Entities\Student;

class ProgressReport
{
    public function __construct(Repository $repo, Student $student)
    {
        $this->repo    = $repo;
        $this->student = $student;
    }

    public function countCompleted()
    {
        return count($this->answeredQuestions());
    }

    public function answeredQuestions()
    {
        return $this->repo->answersForStudent($this->student);
    }
}
