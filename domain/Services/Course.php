<?php

namespace Sqola\Services;

use Sqola\Entities\Unit;
use Sqola\Entities\Lesson;
use Sqola\Entities\NullLesson;
use Sqola\Contracts\Repository;
use Sqola\Values\CourseConfig;

class Course
{
    private $units = [];
    private $index = false;

    public function __construct(Repository $repo, CourseConfig $config)
    {
        $this->repo = $repo;
        $this->units = $config->config;
    }

    public function addUnit(Unit $unit)
    {
        $this->units[] = $unit;
    }

    public function start()
    {
        return $this->lessonAtIndex(0);
    }

    public function setLesson(Lesson $lesson)
    {
        foreach ($this->units as $i => $unit) {
            if ($unit->equals($lesson->unit)) {
                \Log::error('setting lesson to index '.$i);
                $this->index = $i;
            }
        }
    }

    public function lessonAtIndex($index): Lesson
    {
        if (!isset($this->units[$index])) {
            return new NullLesson();
        }

        $unit = $this->units[$index];

        return new Lesson($this->repo, $unit);
    }

    public function nextLesson(): Lesson
    {
        $this->index = $nextIndex = $this->index + 1;

        return $this->lessonAtIndex($nextIndex);
    }
}
