<?php

namespace Sqola\Entities;

class NullLesson extends Lesson
{
    protected $theEnd = true;

    public function __construct()
    {
    }

    public function start(): Problem
    {
        throw new \Exception("Cannot start a null unit.");
    }
}
