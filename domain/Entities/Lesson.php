<?php

namespace Sqola\Entities;

use Sqola\Contracts\Repository;

use Symfony\Component\Process\Process;

class Lesson
{
    private $currentQuestionConfig      = null;
    private $currentQuestionConfigIndex = null;
    protected $theEnd                   = false;

    private $courseConfig = [
        'syntax',
        'loops',
        'functions'
    ];

    public function __construct(Repository $repo, Unit $unit)
    {
        $this->repo = $repo;
        $this->unit = $unit;

        // $this->question = $question;
        // $this->questions = include base_path('resources/units/'.$question->unit->slug.'.php');
        //
        // $this->currentQuestionConfig = $this->questions[$question->configIndex()];

        // $this->student = $student;
        // $this->unit    = $unit;
    }

    public function start(): Problem
    {
        $question = $this->unit->firstQuestion();

        $this->setQuestion($question);

        return $question;
    }

    public function goToQuestion(int $index)
    {
        $question = $this->unit->questionAtIndex($index - 1);

        $this->setQuestion($question);

        return $question;
    }

    public function setQuestion(Problem $question)
    {
        $this->question = $question;
    }

    public function expected()
    {
        return $this->currentQuestion()->expectedOutput();
    }

    public function input()
    {
        return $this->currentQuestion()->input();
    }

    public function preamble()
    {
        return $this->currentQuestion()->preamble() ?? 'Try another one!';
    }

    public function scenario()
    {
        return $this->currentQuestion()->scenario() ?? '';
    }

    public function nextQuestion(): Problem
    {
        if (empty($this->question)) {
            $this->question = $this->unit->firstQuestion();
        } else {
            $this->question = $this->unit->questionAfter($this->question);
        }

        return $this->question;
    }

    public function currentQuestion(): Problem
    {
        if (empty($this->question)) {
            throw new \Exception("This lesson has not been configured with a question.");
        }

        return $this->question;
    }

    public function studentAnswers(Student $student, Problem $problem, $answer)
    {
        // Now have to implement AnswerProblem Command...

        $answer = $student->answer($problem, $answer);
        $this->currentQuestionConfigIndex++;

        return $answer;
    }

    public function run(Answer $answer)
    {
        $php = $answer->answer;

        \Log::error('storing php', [$php]);

        \Storage::put('students/'.$answer->student->id.'/test.php', $php);

        $studentsPath = storage_path('app/students/'.$answer->student->id);
        // $cmd = "/usr/local/bin/docker-compose run php php -r 'echo getcwd();'";

        if (env('PHP_EXECUTION', 'local') == 'docker') {
            $containerName = "s{$answer->student->id}".str_random(20);
            $cmd = "/usr/local/bin/docker-compose run --rm --name $containerName php php {$answer->student->id}/test.php";
        } else {
            $cmd = "/usr/local/bin/php $studentsPath/test.php";
        }

        // \Log::error('base_path()', [base_path()]);
        // \Log::error('cmd', [$cmd]);
        $process = new Process($cmd, base_path());
        $process->run();

        $output = $process->getOutput();
        $error  = $process->getErrorOutput();
        // \Log::error('getting output', [$output]);
        // \Log::error('getting error', [$error]);

        return $output;
    }

    public function incrementIndex()
    {
        $this->currentQuestionConfigIndex++;
    }

    public function isTheEnd()
    {
        return $this->theEnd;
    }
}
