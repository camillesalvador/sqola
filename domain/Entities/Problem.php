<?php

namespace Sqola\Entities;

class Problem
{
    protected $theEnd = false;

    public function __construct($id, Unit $unit, $configId, $title)
    {
        $this->id       = $id;
        $this->unit     = $unit;
        $this->configId = $configId;
        $this->title    = $title;
    }

    public function equals(Problem $question)
    {
        return $this->id == $question->id;
    }

    public function configIndex()
    {
        $parts = explode('.', $this->configId);

        if (count($parts) != 2) {
            throw new \Exception("Invalid config id '$this->configId' for Problem $this->id");
        }

        return $parts[1];
    }

    public function index()
    {
        $configIndex = $this->configIndex();

        preg_match('/[^\d]+(\d+)/', $configIndex, $matches);

        return $matches[1];
    }

    public function isTheEnd()
    {
        return $this->theEnd;
    }

    public function config()
    {
        return $this->unit->configforQuestion($this);
    }

    public function expectedOutput()
    {
        $config = $this->config();

        $input  = $config['input'] ?? null;
        $calc   = $config['calc'];

        ob_start();
        $result = $calc($input);
        $outputBuffer = ob_get_clean();

        if (empty($result)) {
            return $outputBuffer;
        }

        return $result;
    }

    public function input()
    {
        $config = $this->config();
            # code...
        if (isset($config['input'])) {
            return $config['input'];
        }

        if (isset($config['fileInput'])) {
            $file = resource_path('units/'.$this->unit->slug.'/'.$this->configIndex().'.php');
            if (!file_exists($file)) {
                throw new \Exception("Could not find file: $file");
            }

            return file_get_contents($file);
        }
    }

    public function preamble()
    {
        return $this->config()['preamble'] ?? false;
    }

    public function scenario()
    {
        return $this->config()['scenario'] ?? false;
    }
}
