<?php

// "Expected Results" content and subject matter
// graciously provided by my girlfriend

function func1() {
    echo "and then he saw one of those badguys.";
}

function func2() {
    echo ",";
}

function func3() {
    echo "Spock was traveling";
}

function func4() {
    echo "at the speed of light";
}

function func5() {
    echo " ";
}

// You have these functions at your disposal.
// Try using them by calling them in the order needed,
// in order to end up with the Expected Results.
