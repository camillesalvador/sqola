<?php

// Say you had these 5 values...

$value1 = 1;
$value2 = 2;
$value3 = 3;
$value4 = 4;
$value5 = 5;

// And you had to do this calculation with each one:

// $newValue1 = $value1 + 10 / 2.5 - 1.7 + 2500 * 1000;
// $newValue2 = $value2 + 10 / 2.5 - 1.7 + 2500 * 1000;
// $newValue3 = $value3...
// $newValue4 = ...

// and THEN you wanted to sum all of those subsequent values!...

// Ew. Gross.

// Instead of writing it over and over, put it in a function!

function dry($input) {
    return $input + 10 / 2.5 - 1.7 + 2500 * 1000;
}

$newValue1 = dry($value1);

// That's the first one, how would you get the rest?

$sum = // add up the new values!

// Don't forget to print it to the screen.

// The cool thing is, that with loops - you can make this even shorter!
