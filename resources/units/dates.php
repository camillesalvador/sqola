<?php

return [
    'dates1' => [
        'scenario' => "On Mars it's easy to lose track of time. That's why we use PHP to keep track of the date on distant Earth.",
        'preamble' => "Now, use some PHP magic to print out today's date in different formats. (See the expected results.) <br><br>You might find it helpful to google 'php date format'.",
        'calc'     => function ($input) {
            echo date_create()->format('Y-m-d ga')."\n";
            echo date_create()->format('D F jS, Y')."\n";
            echo date_create()->format('l, F jS, \i\n \t\h\e \h\i\p \y\e\a\r Y')."\n";
        },
        'fileInput' => true
    ],
];
