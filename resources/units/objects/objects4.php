<?php

class Car
{
    public $colour = "";
    public $numberOfWheels = 4;

    public function introduceYourself()
    {
        ????

        // hint: to access an Object's own properties or methods,
        // you'll have to use the $this->_______ notation
    }
}

// We currently have an instance of a Car with these properties:
$informativeCar = new Car;
$informativeCar->colour = "Hot Pink";

// You can see a method on the Car Object called "introduceYourself()".

// Can you finish writing that method so that when you tell your
// $informativeCar to introduceYourself(), that it outputs the Expected Result?

echo $informativeCar->introduceYourself();
