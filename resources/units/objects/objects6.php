<?php

class Square {
  // this is the square's property
  public $sides = 4;
}

// an instance of square
$square = new Square();

// create an if statement that echos the expected results
// if the square has a property of sides
