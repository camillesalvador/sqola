@extends('layouts.app')

@section('content')
    <div class="padding-10">

        <h3>Create Question</h3>

        <form class="form-horizontal" method="POST" action="{{ route('questions.post') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="title" class="col-md-4 control-label">Title</label>

                <div class="col-md-6">
                    <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>

                    @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('config_id') ? ' has-error' : '' }}">
                <label for="config_id" class="col-md-4 control-label">
                    Config ID
                </label>

                <div class="col-md-6">
                    <input id="config_id" type="text" class="form-control" name="config_id" value="{{ old('config_id') }}" required autofocus>
                </div>
            </div>

            <div class="form-group{{ $errors->has('unit_id') ? ' has-error' : '' }}">
                <label for="unit_id" class="col-md-4 control-label">
                    Unit
                </label>

                <div class="col-md-6">
                    <select id="unit_id" class="form-control select" name="unit_id" value="{{ old('unit_id') }}" required autofocus>
                        @foreach ($units as $unit)
                            <option value="{{ $unit->id }}">
                                {{ $unit->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Save
                    </button>
                </div>
            </div>
        </form>

    </div>

@endsection
