@extends('layouts.app')

@section('content')

    <div id="app" class="full-height">
        <editor
            expected-result="{{ $expected }}"
            input="{{ $input }}"
            preamble="{{ $preamble }}"
            starting-point="{{ $startingPoint }}"
            scenario="{{ $scenario }}">
        </editor>
    </div>

@endsection

@section('footer')
    <div>
        <h3 class="margin-top-10 margin-bottom-10">Current Unit: {{ $unit }}</h3>
    </div>
@endsection
