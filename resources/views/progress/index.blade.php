@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="students table">
            @foreach ($report->students as $id => $student)
                <tr class="s{{ $id }}">
                    <td class="name">{{ $student['student'] }}</td>
                    <td class="count">{{ $student['count'] }}</td>
                    <td><a href="{{ route('progress.show', $id) }}">view</a></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
