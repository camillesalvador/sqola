<?php

namespace App\Tests\Helpers;

use PHPUnit\Framework\Assert;

trait PHPUnit
{

    public function __call($method, $args)
    {
        if (method_exists(Assert::class, $method)) {
            Assert::$method(...$args);
        }
    }
}
