<?php

namespace App\Tests\Pages;

class Home extends Page
{

    public function assertUnits($units)
    {
        foreach ($units as $unit) {
            $this->assertPageContainsText($unit);
        }
    }

    public function selectUnit($unit)
    {
        $this->clickLink($unit);
    }
}
