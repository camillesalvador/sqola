<?php

namespace App\Tests\Pages;

class Page
{
    public function __construct($context)
    {
        $this->context = $context;
    }

    public function __call($method, $args)
    {
        return $this->context->$method(...$args);
    }
}
