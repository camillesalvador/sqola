<?php

namespace App\Tests\Pages;

class Unit extends Page
{

    public function submitCode($code)
    {
        $this->fillField('code', $code);
        $this->pressButton('submit', false);
    }

    public function verifyOutput($expected)
    {

    }

    public function assertUnit($title)
    {
        $this->assertPageContainsText($title);
    }
}
