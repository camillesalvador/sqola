<?php

namespace App\Tests\Repositories;

use Sqola\Contracts\Repository;
use Sqola\Entities\Answer;
use Sqola\Entities\Problem;
use Sqola\Entities\Student;
use Sqola\Entities\Unit;
use Sqola\Values\UnitConfig;

class InMemoryRepo implements Repository
{
    private $students = [];
    private $questions = [];
    private $answers  = [];

    public function saveStudent(Student $student)
    {
        $this->students[] = $student;
    }

    public function createProblem($payload): Problem
    {
        extract($payload);
        $id   = rand(1, 10000);
        $unit = $this->unitWithSlug($unitSlug);

        $question = new Problem($id, $unit, $configId, $title);
        $this->questions[$question->id] = $question;

        $unit->addQuestion($question);

        return $question;
    }

    public function createUnit($unitName, UnitConfig $config): Unit
    {
        $unitId        = rand(1, 10000);
        $unit = $this->units[] = new Unit($unitId, $config, $unitName, str_slug($unitName));

        return $unit;
    }

    public function studentWithId($id)
    {
        foreach ($this->students as $student) {
            if ($student->id == $id) {
                return $student;
            }
        }

        throw new \Exception("Student with id '$id' not found.");
    }

    // public function problemWithTitle($title)
    // {
    //     return $this->questions[$title] ?? false;
    // }

    public function problemWithId($id)
    {
        foreach ($this->questions as $problem) {
            if ($problem->id == $id) {
                return $problem;
            }
        }

        throw new \Exception("Problem with id '$id' not found.");
    }

    public function answersForStudent(Student $student)
    {
        return $this->answers[$student->id];
    }

    public function saveAnswer(Answer $answer)
    {
        $this->answers[$answer->student->id][] = $answer;
    }

    public function questionsWithUnitId($id)
    {
        # code...
    }

    public function unitWithSlug($unitSlug): Unit
    {
        foreach ($this->units as $unit) {
            if ($unit->slug == $unitSlug) {
                return $unit;
            }
        }

        throw new \Exception("Could not retrieve unit with slg: '$unitSlug'");
    }

    public function questionWithConfigId($configId): Problem
    {
        foreach ($this->questions as $problem) {
            if ($problem->configId == $configId) {
                return $problem;
            }
        }

        throw new \Exception("Could not retrieve problem with configId: '$configId'");
    }

    public function configForUnit(Unit $unit)
    {
        return new UnitConfig([
            'syntax1' => [],
            'syntax2' => [],
        ]);
    }
}
