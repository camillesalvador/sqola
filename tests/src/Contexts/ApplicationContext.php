<?php

namespace App\Tests\Contexts;

use App\Factories\DatabaseProblemFactory;
use App\Factories\DatabaseUnitFactory;
use App\Factories\DatabaseUserFactory;
use App\Repositories\DbRepo;

use App\Tests\Helpers\PHPUnit;
use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Testwork\Hook\Scope\AfterSuiteScope;
use Behat\Testwork\Hook\Scope\BeforeSuiteScope;
use Faker\Factory;
use Illuminate\Support\Facades\Artisan;
use Sqola\Commands\AnswerProblem;
use Sqola\Commands\CreateProblem;
use Sqola\Commands\CreateUnit;
use Sqola\Commands\GetAllStudentReports;
use Sqola\Commands\GetLesson;
use Sqola\Commands\GetQuestion;
use Sqola\Commands\GetStudentReport;
use Sqola\Commands\GetUnitQuestions;
use Sqola\Commands\MakeStudent;
use Sqola\Values\UnitConfig;

/**
 * Defines application features from the specific context.
 */
class ApplicationContext extends MinkContext implements Context
{
    use PHPUnit;
    use \Laracasts\Behat\Context\DatabaseTransactions;

    public $studentIds = [];
    public $problemIds = [];
    public $unitIds    = [];

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct($repoContext = 'memory')
    {
        $this->faker = Factory::create();
        $this->repo  = new DbRepo();
    }

    /** @BeforeSuite */
    public static function setup(BeforeSuiteScope $scope)
    {
        // \Log::error('App Context — copying .env');
        // copy('.env', '.env.temp');
        // copy('.env.behat', '.env');
        \Log::error('App Context — resetting database');
        Artisan::call('migrate:fresh');
    }

    /** @AfterSuite */
    public static function teardown(AfterSuiteScope $scope)
    {
        // \Log::error('App Context — copying .env back');
        // copy('.env.temp', '.env');
    }

    /**
     * @Given there is a student :arg1
     */
    public function thereIsAStudent($studentName)
    {
        $userFactory = new DatabaseUserFactory;
        $makeStudent = new MakeStudent($this->repo, $userFactory);
        $id          = $makeStudent->execute([
            'name'     => $studentName,
            'email'    => $this->faker->email,
            'password' => ''
        ]);

        $this->studentIds[] = $id;
    }

    // /**
    //  * @Given student :arg1 has completed :arg2 questions
    //  */
    // public function studentHasCompletedQuestions($index, $num)
    // {
    //     for ($i=0; $i < $num; $i++) {
    //         $problem = new CreateProblem($this->repo);
    //         $problem->execute([
    //             'title' => 'Title'.$i
    //         ]);
    //
    //         $answer = new AnswerProblem($this->repo);
    //         $answer->execute([
    //             'id' => $this->studentIds[$index - 1],
    //             'answer' => '42',
    //             'problem' => $problemId
    //         ]);
    //     }
    // }

    /**
     * @When an admin views the progress area
     */
    public function anAdminViewsTheProgressArea()
    {
    }

    /**
     * @Then student :arg1 shows :arg2 questions completed
     */
    public function studentShowsQuestionsCompleted($index, int $num)
    {
        $reports   = new GetAllStudentReports($this->repo);
        $response  = $reports->execute();
        $studentId = $this->studentIds[$index - 1];

        $this->assertEquals($num, $response->students[$studentId]['count']);
    }

    /**
     * @Given student :arg1 has completed the following questions
     */
    public function studentHasCompletedTheFollowingQuestions($index, TableNode $table)
    {
        $problemFactory = new DatabaseProblemFactory;
        $studentId      = $this->studentIds[$index - 1];

        foreach ($table->getHash() as $row) {
            $createProblem = new CreateProblem($this->repo, $problemFactory);
            $problemId     = $createProblem->execute([
                'unit_id'   => $row['Unit ID'],
                'title'     => $row['Question'],
                'config_id' => 'syntax.syntax1'
            ]);

            $this->problemIds[] = $problemId;
        }

        \Log::error('problemIds', $this->problemIds);

        foreach ($table->getHash() as $i => $row) {
            $getQuestion = new GetQuestion($this->repo);
            $question    = $getQuestion->execute([
                'question_id' => $this->problemIds[$i]
            ]);

            \Log::error('2222', [
                $i, $question
            ]);

            $answer = new AnswerProblem($this->repo);
            $answer->execute([
                'student_id'   => $studentId,
                'answer'       => $row['Answer'],
                'problem_id'   => $question['id'],
                'unit_slug'    => 'syntax',
                'lesson_index' => $question['id']
            ]);
        }
    }

    /**
     * @Then the report for student :arg1 shows:
     */
    public function theReportForStudentShows($index, TableNode $table)
    {
        $studentReport = new GetStudentReport($this->repo);

        $answeredQuestions = $studentReport->execute([
            'id' => $this->studentIds[$index - 1],
        ]);

        foreach ($table->getHash() as $row) {
            $hasAnswer = $this->answersContain(
                $answeredQuestions->answeredQuestions,
                $row
            );

            if ($hasAnswer) {
                $found = true;
            } else {
                throw new \Exception(
                    "Question {$row['Question']} with answer {$row['Answer']} not found: ".print_r($answeredQuestions, true)
                );
            }
        }
    }

    public function answersContain($answeredQuestions, $row)
    {
        foreach ($answeredQuestions as $answer) {
            if ($answer->answer == $row['Answer'] && $answer->question == $row['Question']) {
                return true;
            }
        }

        return false;
    }

    /**
     * @When I change question :arg1 to :arg2
     */
    public function iChangeQuestionTo($title, $newTitle)
    {
        foreach ($this->problemIds as $id) {
            $existingProblem = $this->repo->problemWithId($id, $title);
            if ($existingProblem->title == $title) {
                $existingProblem->title = $newTitle;
                $this->repo->updateProblem($existingProblem);
            }
        }
    }

    /**
     * @Given there are the following units
     */
    public function thereAreTheFollowingUnits(TableNode $table)
    {
        $unitFactory = new DatabaseUnitFactory;

        foreach ($table->getHash() as $row) {
            $createUnit = new CreateUnit($this->repo, $unitFactory);
            $unitId     = $createUnit->execute([
                'name' => $row['Name'],
                'slug' => str_slug($row['Name'])
            ]);

            $this->unitIds[] = $unitId;
        }
    }

    /**
     * @Given there is a unit :arg1 with questions
     */
    public function thereIsAUnitWithQuestions($unitName)
    {
        $unitFactory = new DatabaseUnitFactory;

        $createUnit = new CreateUnit($this->repo, $unitFactory);
        $unitId     = $createUnit->execute([
            'name' => $unitName,
            'slug' => str_slug($unitName)
        ]);

        $this->unitIds[] = $unitId;


        for ($i=1; $i < 6; $i++) {
            $problemFactory = new DatabaseProblemFactory;
            $createProblem  = new CreateProblem($this->repo, $problemFactory);
            $problemId      = $createProblem->execute([
                'unit_id'   => $unitId,
                'config_id' => 'configId'.$i,
                'title'     => 'questionTitle'.$i
            ]);

            $this->problemIds[] = $problemId;
        }
    }

    /**
     * @When I log in as a student
     */
    public function iLogInAsAStudent()
    {
        $this->thereIsAStudent('studentName');
    }

    /**
     * @When I answer all the questions for unit :arg1
     */
    public function iAnswerAllTheQuestionsForUnit($unitName)
    {
        // get unit by $unitName

        $studentId = $this->studentIds[0];

        $getUnitQuestions = new GetUnitQuestions($this->repo);
        $questions        = $getUnitQuestions->execute([
            'unit_id' => $this->unitIds[0]
        ]);

        foreach ($questions as $question) {
            $answer = new AnswerProblem($this->repo);
            $answer->execute([
                'student_id' => $studentId,
                'answer'     => 'studentAnswer',
                'problem_id' => $question->id
            ]);
        }
    }

    /**
     * @Then the admin can see my progress
     */
    public function theAdminCanSeeMyProgress()
    {
        $studentReport = new GetStudentReport($this->repo);

        $response = $studentReport->execute([
            'id' => $this->studentIds[0],
        ]);

        foreach ($response->answeredQuestions as $answer) {
            $hasAnswer = (
                $answer->answer == 'studentAnswer'
            );

            if ($hasAnswer) {
                $found = true;
            } else {
                throw new \Exception(
                    "Questions with answer 'studentAnswer' not found: ".print_r($answeredQuestions, true)
                );
            }
        }
    }

    /**
     * @Given there is a unit :arg1 with :arg2 questions
     */
    public function thereIsAUnitWithQuestions2($unitName, $numberOfQuestions)
    {
        $config = [];
        for ($i=1; $i <= $numberOfQuestions; $i++) {
            $configId          = $unitName.$i;
            $config[$configId] = [];
        }

        $createUnit = new CreateUnit($this->repo, new UnitConfig($config));

        $unitId     = $createUnit->execute([
            'name' => $unitName,
        ]);

        $this->unitIds[] = $unitId;

        for ($i=1; $i <= $numberOfQuestions; $i++) {
            $problemFactory = new DatabaseProblemFactory;
            $createProblem  = new CreateProblem($this->repo, $problemFactory);
            $problemId      = $createProblem->execute([
                'unit_id'   => $unitId,
                'config_id' => $unitName.'.'.$unitName.$i,
                'title'     => 'questionTitle'.$i
            ]);

            $this->problemIds[] = $problemId;
        }
    }

    /**
     * @When I start a lesson
     */
    public function iStartALesson()
    {
        $studentId  = $this->studentIds[0];

        // $startLesson = new StartLesson($this->repo);
        // $startLesson->execute();

        $this->next = (object) [
            'unit'   => 'syntax',
            'lesson' => '1'
        ];
    }

    /**
     * @Then I can see question :arg2 from unit :arg1
     */
    public function iCanSeeQuestionFromUnit($questionIndex, $unitName)
    {
        \Log::error('getting lesson...', [$this->next->unit, $this->next->lesson]);
        $getLesson = new GetLesson($this->repo);
        $lesson    = $getLesson->execute([
            'studentId'   => $this->studentIds[0],
            'unitName'    => $this->next->unit,
            'lessonIndex' => $this->next->lesson
        ]);
        \Log::error('got lesson...', [$lesson['unit'], $lesson['lesson']]);

        $this->assertEquals(
            [$unitName, $questionIndex],
            [$lesson['unit'], $lesson['lesson']]
        );
    }

    /**
     * @Then I answer question :arg2 for unit :arg1
     */
    public function iAnswerQuestionForUnit($questionIndex, $unitName)
    {
        $studentId  = $this->studentIds[0];

        $correctAnswers = [
            'syntax.syntax1' => 'echo "Manual Override Function Enabled.";',
            'syntax.syntax2' => '$i = "D F jS, Y"; echo date($i);',
            'loops.loops1'   => 'echo "Manual Override Function Enabled.";',
            'loops.loops2'   => '$i = "D F jS, Y"; echo date($i);',
        ];

        $pureAnswer = $correctAnswers[$unitName.'.'.$unitName.$questionIndex];

        $answer = "<?php\n\n" . $pureAnswer;


        $answerProblem = new AnswerProblem($this->repo);
        $response      = $answerProblem->execute([
            'student_id'    => $studentId,
            'answer'        => $answer,
            'unitSlug'      => $unitName,
            'questionIndex' => $questionIndex,
        ]);

        $this->next = (object) [
            'unit'   => $response['unit'] ?? false,
            'lesson' => $response['lesson'] ?? false
        ];
    }

    /**
     * @Then I am done
     */
    public function iAmDone()
    {
        $this->assertFalse($this->next->unit);
        $this->assertFalse($this->next->lesson);
    }
}
