Feature: Questions are attached to Units

@domain @app @api @browser
Scenario: Student answers a question from a unit
    Given there is a student "a"
    And there is a student "b"
    And there are the following units
    | Id | Name |
    | 1  | One  |
    | 2  | Two  |
    And student 1 has completed the following questions
    | Unit ID | Question                    | Answer    |
    | 1       | what is the meaning of life | 42        |
    | 1       | what is 2 + 2               | 4         |
    | 1       | what is peanut butter       | delicious |
    And student 1 has completed the following questions
    | Unit ID | Question                    | Answer    |
    | 2       | what is the meaning of life | 42        |
    | 2       | what is 2 + 2               | 4         |
    | 2       | what is peanut butter       | delicious |
    Then the report for student 1 shows:
    | Unit ID | Question                    | Answer    |
    | 1       | what is the meaning of life | 42        |
    | 1       | what is 2 + 2               | 4         |
    | 1       | what is peanut butter       | delicious |
    | 2       | what is the meaning of life | 42        |
    | 2       | what is 2 + 2               | 4         |
    | 2       | what is peanut butter       | delicious |
