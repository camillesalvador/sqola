Feature: Students can progress through lessons

@domain @app @api
Scenario: Student progresses through a lesson
    Given there is a unit "syntax" with 2 questions
    And there is a unit "loops" with 2 questions
    When I log in as a student
    And I start a lesson
    Then I can see question 1 from unit "syntax"
    And I answer question 1 for unit "syntax"
    Then I can see question 2 from unit "syntax"
    And I answer question 2 for unit "syntax"
    Then I can see question 1 from unit "loops"
    And I answer question 1 for unit "loops"
    Then I can see question 2 from unit "loops"
    And I answer question 2 for unit "loops"
    Then I am done
