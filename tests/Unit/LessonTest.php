<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Tests\Repositories\InMemoryRepo;
use Sqola\Entities\Lesson;
use Sqola\Entities\NullQuestion;
use Sqola\Values\UnitConfig;
use Sqola\Services\Course;

class LessonTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $repo = new InMemoryRepo;

        $unitConfig = new UnitConfig([
            'syntax1' => [],
            'syntax2' => [],
        ]);

        $syntax = $repo->createUnit('syntax', $unitConfig);

        $unitConfig = new UnitConfig([
            'loops1' => [],
            'loops2' => [],
        ]);

        $loops = $repo->createUnit('loops', $unitConfig);

        $question1 = $repo->createProblem([
            'unitSlug' => 'syntax',
            'configId' => 'syntax.syntax1',
            'title' => 'haha'
        ]);
        $question2 = $repo->createProblem([
            'unitSlug' => 'syntax',
            'configId' => 'syntax.syntax2',
            'title' => 'haha'
        ]);
        $question3 = $repo->createProblem([
            'unitSlug' => 'loops',
            'configId' => 'loops.loops1',
            'title' => 'haha'
        ]);
        $question4 = $repo->createProblem([
            'unitSlug' => 'loops',
            'configId' => 'loops.loops2',
            'title' => 'haha'
        ]);

        $lesson = new Lesson($repo, $syntax);
        $lesson->start();

        $next = $lesson->nextQuestion();

        $this->assertEquals(2, $next->index());
        $this->assertEquals('syntax', $next->unit->name);

        $next = $lesson->nextQuestion();

        $this->assertInstanceOf(NullQuestion::class, $next);

        $lesson->setQuestion($question1);
        $next = $lesson->nextQuestion();
        $this->assertEquals($next, $question2);
    }
}
