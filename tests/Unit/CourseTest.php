<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Tests\Repositories\InMemoryRepo;
use Sqola\Entities\Lesson;
use Sqola\Entities\NullLesson;
use Sqola\Entities\NullQuestion;
use Sqola\Values\UnitConfig;
use Sqola\Values\CourseConfig;
use Sqola\Services\Course;

class CourseTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $repo = new InMemoryRepo;

        $unitConfig = new UnitConfig([
            'syntax1' => [],
            'syntax2' => [],
        ]);

        $syntax = $repo->createUnit('syntax', $unitConfig);

        $unitConfig = new UnitConfig([
            'loops1' => [],
            'loops2' => [],
        ]);

        $loops = $repo->createUnit('loops', $unitConfig);

        $question1 = $repo->createProblem([
            'unitSlug' => 'syntax',
            'configId' => 'syntax.syntax1',
            'title' => 'haha'
        ]);
        $question2 = $repo->createProblem([
            'unitSlug' => 'syntax',
            'configId' => 'syntax.syntax2',
            'title' => 'haha'
        ]);
        $question3 = $repo->createProblem([
            'unitSlug' => 'loops',
            'configId' => 'loops.loops1',
            'title' => 'haha'
        ]);
        $question4 = $repo->createProblem([
            'unitSlug' => 'loops',
            'configId' => 'loops.loops2',
            'title' => 'haha'
        ]);

        $config = new CourseConfig([
            $syntax,
            $loops
        ]);

        $course = new Course($repo, $config);

        $syntaxLesson = $course->start();

        $this->assertEquals('syntax', $syntaxLesson->unit->name);

        $question = $syntaxLesson->start();
        $this->assertEquals('syntax1', $question->configIndex());

        $question = $syntaxLesson->nextQuestion();
        $this->assertEquals('syntax2', $question->configIndex());

        $loopsLesson = $course->nextLesson();

        $this->assertEquals('loops', $loopsLesson->unit->name);

        $question = $loopsLesson->start();
        $this->assertEquals('loops1', $question->configIndex());

        $question = $loopsLesson->nextQuestion();
        $this->assertEquals('loops2', $question->configIndex());

        $nullLesson = $course->nextLesson();

        $this->assertInstanceOf(NullLesson::class, $nullLesson);

        $course->setLesson($syntaxLesson);
        $next = $course->nextLesson();

        $this->assertEquals('syntax', $syntaxLesson->unit->name);
    }


}
